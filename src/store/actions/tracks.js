import axios from '../../axios-api';
import {FETCH_TRACKS_SUCCESS} from "./actionTypes";

const fetchTracksSuccess = tracks => {
  return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = id => {
  return dispatch => {
    axios.get('/tracks/' + id).then(
      response => dispatch(fetchTracksSuccess(response.data))
    )
  };
};