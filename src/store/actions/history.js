import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {FETCH_HISTORY_SUCCESS} from "./actionTypes";

const fetchHistorySuccess = tracks => {
  return {type: FETCH_HISTORY_SUCCESS, tracks};
};

export const fetchHistory = () => {
  return (dispatch, getState) => {
    let token = null;
    if (getState().users.user) token = getState().users.user.token;
    const headers = {'Token': token};
    return axios.get('/history', {headers}).then(
      response => {
        dispatch(fetchHistorySuccess(response.data));
      },
      error => {
        dispatch(push('/login'));
      }
    )
  };
};

export const addToHistory = id => {
  return (dispatch, getState) => {
    let token = null;
    if (getState().users.user) token = getState().users.user.token;
    return axios.post('/history/' + id, {token}).then(
      response => {
        console.log('Success');
      },
      error => {
        dispatch(push('/login'));
      }
    )
  };
};