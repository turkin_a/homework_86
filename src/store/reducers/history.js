import {FETCH_HISTORY_SUCCESS} from "../actions/actionTypes";

const initialState = {
  tracks: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_HISTORY_SUCCESS:
      return {...state, tracks: action.tracks};
    default:
      return state;
  }
};

export default reducer;