import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const HistoryListItem = props => {
  return (
    <Panel>
      <Panel.Body>
        <p>{props.datetime}</p>
        <p><strong>{props.artist} - {props.name}</strong></p>
      </Panel.Body>
    </Panel>
  );
};

HistoryListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  datetime: PropTypes.string.isRequired
};

export default HistoryListItem;