import React from 'react';
import {Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const TracksListItem = props => {
  return (
    <Panel>
      <Panel.Body>
        <p>Track #{props.number}</p>
        <p>
          <strong style={{marginRight: '15px', cursor: 'pointer'}} onClick={props.clicked}>{props.name}</strong>
          {props.duration} sec
        </p>
      </Panel.Body>
    </Panel>
  );
};

TracksListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  album: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired
};

export default TracksListItem;