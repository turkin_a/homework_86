import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import notFound from '../../assets/images/not_found.png';
import config from "../../config";

const ArtistsListItem = props => {
  let photo = notFound;

  if (props.photo) {
    photo = config.apiUrl + '/uploads/' + props.photo;
  }

  return (
    <Panel>
      <Panel.Body>
        <Image
          style={{width: '100px', marginRight: '10px', float: 'left'}}
          src={photo}
          thumbnail
        />
        <Link to={'/albums/' + props.id}>
          <strong>{props.name}</strong>
        </Link>
      </Panel.Body>
    </Panel>
  );
};

ArtistsListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired
};

export default ArtistsListItem;