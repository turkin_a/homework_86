import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {fetchTracks} from "../../store/actions/tracks";
import TracksListItem from "../../components/TracksListItem/TracksListItem";
import {addToHistory} from "../../store/actions/history";

class Tracks extends Component {
  componentDidMount() {
    this.props.onFetchTracks(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Tracks
        </PageHeader>
        {this.props.tracks.map(track => (
          <TracksListItem
            key={track._id}
            id={track._id}
            name={track.name}
            album={track.album}
            duration={track.duration}
            number={track.number}
            clicked={() => this.props.addToHistory(track._id)}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    tracks: state.tracks.tracks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchTracks: (id) => dispatch(fetchTracks(id)),
    addToHistory: (id) => dispatch(addToHistory(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);