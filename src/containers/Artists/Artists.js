import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ArtistsListItem from "../../components/ArtistsListItem/ArtistsListItem";
import {fetchArtists} from "../../store/actions/artists";

class Artists extends Component {
  componentDidMount() {
    this.props.onFetchArtists();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Artists
        </PageHeader>
        {this.props.artists.map(artist => (
          <ArtistsListItem
            key={artist._id}
            id={artist._id}
            name={artist.name}
            photo={artist.photo}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);