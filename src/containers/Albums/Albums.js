import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {fetchAlbums} from "../../store/actions/albums";
import AlbumsListItem from "../../components/AlbumsListItem/AlbumsListItem";

class Albums extends Component {
  componentDidMount() {
    this.props.onFetchAlbums(this.props.match.params.id);
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Albums
        </PageHeader>
        {this.props.albums.map(album => (
          <AlbumsListItem
            key={album._id}
            id={album._id}
            title={album.title}
            artist={album.artist}
            year={album.year}
            poster={album.poster}
          />
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    albums: state.albums.albums
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchAlbums: (id) => dispatch(fetchAlbums(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);